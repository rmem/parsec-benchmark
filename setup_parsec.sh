#!/bin/bash

prefix=/tmp/parsec
if [ -n "$1" ]; then
    prefix="$1"
fi

if [ ! -e "$prefix" ] || [ ! -e "$prefix/prepare.sh" ]; then
    rm -rf "$prefix"  2>/dev/null
    git clone https://gitlab.inf.telecom-sudparis.eu/rmem/parsec-benchmark.git $prefix
fi

cd "$prefix"
bash ./prepare.sh
