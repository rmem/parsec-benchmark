#!/bin/bash

prefix=$(dirname $(realpath $0))
echo $prefix

cd $prefix

source env.sh

sleep_duration=30
ncore=$(lstopo --only core|wc -l)
niter=5
pb_size=native


parsec_apps="parsec.bodytrack
parsec.canneal
parsec.dedup
parsec.ferret
parsec.fluidanimate
parsec.freqmine
parsec.streamcluster"


#parsec_apps="parsec.bodytrack
#parsec.canneal
#parsec.cmake
#parsec.dedup
#parsec.facesim
#parsec.ferret
#parsec.fluidanimate
#parsec.freqmine
#parsec.glib
#parsec.gsl
#parsec.hooks
#parsec.libjpeg
#parsec.libtool
#parsec.libxml2
#parsec.mesa
#parsec.parmacs
#parsec.raytrace
#parsec.ssl
#parsec.streamcluster
#parsec.swaptions
#parsec.tbblib
#parsec.uptcpip
#parsec.vips
#parsec.x264
#parsec.yasm
#parsec.zlib"

splash2_apps="splash2.barnes
splash2.cholesky
splash2.fft
splash2.fmm
splash2.lu_cb
splash2.lu_ncb
splash2.ocean_cp
splash2.ocean_ncp
splash2.radiosity
splash2.radix
splash2.raytrace
splash2.volrend
splash2.water_nsquared
splash2.water_spatial
splash2x.barnes
splash2x.cholesky
splash2x.fft
splash2x.fmm
splash2x.lu_cb
splash2x.lu_ncb
splash2x.ocean_cp
splash2x.ocean_ncp
splash2x.radiosity
splash2x.radix
splash2x.raytrace
splash2x.volrend
splash2x.water_nsquared
splash2x.water_spatial"

apps="$parsec_apps"
#apps="parsec.streamcluster"

modes="interleaved first_touch"

NUMA_NODES=$(numactl --show|grep membind|sed 's/membind: //'| sed 's/ $//' |sed 's/ /,/g')

run_app() {
    app=$1
    mode=$2
    pb_size=$3
    iter=$4

    cmd="parsecmgmt -a run -p $app -i $pb_size -n $ncore"

    date=$(date +%F_%R:%S)
    log_file_stdout=logs/${app}_${mode}_${pb_size}_${iter}_stdout_$date.log
    log_file_stderr=logs/${app}_${mode}_${pb_size}_${iter}_stderr_$date.log

    numa_log1=logs/${app}_${mode}_${pb_size}_${iter}_${date}_numastat1.log
    numa_log2=logs/${app}_${mode}_${pb_size}_${iter}_${date}_numastat2.log
    numa_log_diff=logs/${app}_${mode}_${pb_size}_${iter}_${date}_numastat_diff.log

    echo "Running $app with mode $mode (pb_size: $pb_size, iter $iter)"
    numastat > $numa_log1

    set -x
    if [ "$mode" = interleaved ]; then
	#	time numactl -i $NUMA_NODES likwid-pin -c $cpus $cmd
	time numactl -i $NUMA_NODES $cmd
    elif [ "$mode" = first_touch ]; then
	time  $cmd
    fi > $log_file_stdout 2> $log_file_stderr
    set +x
    numastat > $numa_log2
    grep real $log_file_stdout $log_file_stderr
}

mkdir logs 2>/dev/null

for mode in $modes ; do
    for iter in $(seq $niter); do
	for app in $apps ; do
	    run_app $app $mode $pb_size $iter
	    sleep $sleep_duration
	done
    done
done

