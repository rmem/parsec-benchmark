#!/bin/bash

prefix=$(dirname $(realpath $0))
cd $prefix

source env.sh
bash ./download_dataset.sh

apt install build-essential m4 libx11-dev automake autoconf libxext-dev libxt-dev libxmu-headers libxi-dev libxmu6 libxmu-dev pkg-config gettext

parsecmgmt -a build

