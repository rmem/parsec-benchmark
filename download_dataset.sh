#!/bin/bash

prefix=$(dirname $(realpath $0))
DEST_DIR=$prefix

nb_dataset=$(find . -name inputs |wc -l)

if [ "$nb_dataset" -lt 10 ]; then
    wget http://parsec.cs.princeton.edu/download/3.0/parsec-3.0-input-native.tar.gz
    tar xf parsec-3.0-input-native.tar.gz
    cp -r parsec-3.0/* $DEST_DIR
fi


